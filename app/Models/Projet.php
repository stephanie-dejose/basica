<?php
// ./app/Models/Projet.php
namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class Projet extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'projets';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    protected $fillable = ['titre', 'description', 'image', 'client', 'slider'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    // this is a recommended way to declare event handlers
    public static function boot()
   {
       parent::boot();

       // Attach event handler, on deleting of the article
       Projet::deleting(function($projet)
       {
           $projet->tags()->detach();
       });
   }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function tags() {
      return $this->belongsToMany('App\Models\Tag', 'projet_tag', 'projet_id', 'tag_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    public function setImageAttribute($value)
     {
         $attribute_name = "image";
         $disk = "img";
         $destination_path = "portfolio";

         // if the image was erased
         if ($value==null) {
             // delete the image from disk
             \Storage::disk($disk)->delete($this->{$attribute_name});

             // set null in the database column
             $this->attributes[$attribute_name] = null;
         }

         // if a base64 was sent, store it in the db
         if (starts_with($value, 'data:image'))
         {
             // 0. Make the image
             $image = \Image::make($value)->encode('jpg', 90);
             // 1. Generate a filename.
             $filename = md5($value.time()).'.jpg';
             // 2. Store the image on disk.
             \Storage::disk($disk)->put($destination_path.'/'.$filename, $image->stream());
             // 3. Save the path to the database
             $this->attributes[$attribute_name] = $filename;
         }
     }
}
