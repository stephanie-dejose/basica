<?php
// ./app/Http/Controllers/PagesController.php

namespace App\Http\Controllers;

use App\Models\Page as PagesModel;
use App\Models\Projet as ProjetsModel;
use App\Models\Post as PostsModel;
use Illuminate\Support\Facades\View;

class PagesController extends Controller {
  /**
   * Détails de la page $id
   * @param integer $id [id de la page à afficher]
   * @return [page]     [Vue pages/show.blade.php]
   */
  public function show($id = 1) {
    $page = PagesModel::find($id);
    if($page->id === 1):
      $projets = ProjetsModel::orderBy('created_at', 'desc')->take(6)->get();
      $posts = PostsModel::orderBy('created_at', 'desc')->take(3)->get();
      return View::make('pages.show', compact('page', 'projets', 'posts'));
    elseif($page->id === 2):
      $projets = ProjetsModel::orderBy('created_at', 'desc')->take(6)->get();
      return View::make('pages.show', compact('page', 'projets'));
    elseif($page->id === 3):
      $posts = PostsModel::orderBy('created_at', 'desc')->paginate(4);
      return View::make('pages.show', compact('page', 'posts'));
    endif;
    return View::make('pages.show', compact('page'));
  }
}
