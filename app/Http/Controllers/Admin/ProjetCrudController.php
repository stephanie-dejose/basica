<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ProjetRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ProjetCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ProjetCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Projet');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/projet');
        $this->crud->setEntityNameStrings('projet', 'projets');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        $this->crud->setFromDb();
        $this->crud->removeButton('show');
        $this->crud->removeColumn('titre');
        $this->crud->addColumn([
          'label' => "Titre",
          'name' => 'titre',
          'type' => 'text',
        ]);
        $this->crud->removeColumn('description');
        $this->crud->addColumn([
          'label' => "Description",
          'name' => 'description',
          'type' => 'text',
        ]);
        $this->crud->removeColumn('client');
        $this->crud->addColumn([
          'label' => "Client",
          'name' => 'client',
          'type' => 'text',
        ]);
        $this->crud->addColumn([
          // n-m relationship tags / montre les tags d'un projet
          'label' => "Tags", // Table column heading
          'type' => "select_multiple",
          'name' => 'tags', // the method that defines the relationship in your model
          'entity' => 'tags', // the method that defines the relationship in your Model
          'attribute' => "nom", // foreign key attribute that is shown to user
          'model' => "App\Models\Tag", // foreign key model
        ]);
        $this->crud->removeColumn('image');
        $this->crud->addColumn([
          // 1-n relationship image / montre l'image d'un projet
          'label' => "Image", // Table column heading
          'type' => "image",
          'name' => 'image', // the column that contains the ID of that connected entity;
          'prefix' => 'img/portfolio/',
          'height' => '100px',
       ]);
       $this->crud->removeColumn('slider');
       $this->crud->addColumn([
         'label' => "Slider",
         'name' => 'slider',
         'type' => 'check',
       ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(ProjetRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        $this->crud->setFromDb();
        $this->crud->addField([
          'type' => 'select2_multiple',
          'name' => 'tags', // the relationship name in your Model
          'entity' => 'tags', // the relationship name in your Model
          'attribute' => 'nom', // attribute on Article that is shown to admin
          'pivot' => true, // on create&update, do you need to add/delete pivot table entries?
        ]);
        $this->crud->removeField('image');
        $this->crud->addField([
          'label' => "Image",
          'name' => "image",
          'type' => 'image',
          'upload' => true,
          'crop' => false, // set to true to allow cropping, false to disable (redimensionner une image,...)
          'aspect_ratio' => 1,
          'prefix' => 'img/portfolio/',
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
