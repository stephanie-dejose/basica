<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PostRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class PostCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class PostCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Post');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/post');
        $this->crud->setEntityNameStrings('post', 'posts');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        $this->crud->setFromDb();
        $this->crud->removeButton('show');
        $this->crud->removeColumn('titre');
        $this->crud->addColumn([
          'label' => "Titre",
          'name' => 'titre',
          'type' => 'text',
        ]);
        $this->crud->removeColumn('texte');
        $this->crud->addColumn([
          'label' => "Texte",
          'name' => 'texte',
          'type' => 'text',
        ]);
        $this->crud->removeColumn('categorie_id');
        $this->crud->addColumn([
            // 1-n relationship catégorie / montre la catégorie d'un post
            'label' => "Catégorie", // Table column heading
            'type' => "select",
            'name' => 'categorie_id', // the column that contains the ID of that connected entity;
            'entity' => 'categorie', // the method that defines the relationship in your Model
            'attribute' => "nom", // foreign key attribute that is shown to user
            'model' => "App\Models\Categorie", // foreign key model
       ]);
       $this->crud->removeColumn('image');
       $this->crud->addColumn([
           // 1-n relationship image / montre l'image d'un post
           'label' => "Image", // Table column heading
           'type' => "image",
           'name' => 'image', // the column that contains the ID of that connected entity;
           'prefix' => 'img/blog/',
           'height' => '100px',
      ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(PostRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        $this->crud->setFromDb();
        $this->crud->removeField('categorie_id');
        $this->crud->addField([
          // Select2
             'label' => "Categorie",
             'type' => 'select2',
             'name' => 'categorie_id', // the db column for the foreign key
             'entity' => 'categorie', // the method that defines the relationship in your Model
             'attribute' => 'nom', // foreign key attribute that is shown to user
             'model' => "App\Models\Categorie", // foreign key model

             // optional
             'options'   => (function ($query) {
                  return $query->orderBy('nom', 'ASC')->get();
              }), // force the related options to be a custom query, instead of all(); you can use this to filter the results show in the select
        ]);
        $this->crud->removeField('image');
        $this->crud->addField([
          'label' => "Image",
          'name' => "image",
          'type' => 'image',
          'upload' => true,
          'crop' => false, // set to true to allow cropping, false to disable (redimensionner une image,...)
          'aspect_ratio' => 1,
          'prefix' => 'img/blog/',
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
