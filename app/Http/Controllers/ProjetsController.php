<?php
// ./app/Http/Controllers/ProjetsController.php

namespace App\Http\Controllers;

use App\Models\Projet;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request; //  Pour récupérer des données GET ou POST

class ProjetsController extends Controller {
  /**
   * Détails du projet
   * @param integer $projet [détails du projet à afficher]
   * @return [show]     [Vue projets/show.blade.php]
   */
  public function show(Projet $projet) {
          $tagIds = $projet->tags->pluck('id')->toArray();

          $projetsSimilaires = Projet::whereHas('tags', function ($query) use ($tagIds) {
            return $query->whereIn('id', $tagIds);})
          ->where('id', '!=', $projet->id)
          ->get();

    return View::make('projets.show', compact('projet', 'projetsSimilaires'));
  }
  /**
   * Chargement des anciens posts
   * @return [latest]      [vue posts/latest.blade.php]
   */
  public function ajaxOlders(Request $request) {
    $projets = Projet::orderBy('created_at', 'desc')
                       ->take(6)
                       ->offset($request->get('offset'))
                       ->get();
    return View::make('projets.liste', compact('projets'));
  }
}
