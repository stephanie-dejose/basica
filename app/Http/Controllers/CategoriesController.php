<?php
// ./app/Http/Controllers/CategoriesController.php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use App\Models\Categorie;

class CategoriesController extends Controller {
  /**
   * Détails d'une catégorie
   * @param  integer $categorie [categorie de la page à afficher]
   * @return [categorie]      [vue categories/show.blade.php]
   */
  public function show(Categorie $categorie) {
    return View::make('categories.show', compact('categorie'));
  }
}
