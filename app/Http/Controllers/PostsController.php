<?php
// ./app/Http/Controllers/PostsController.php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;

class PostsController extends Controller {
  /**
   * Détails de la page $id
   * @param integer $post [détails du post à afficher]
   * @return [post]     [Vue posts/show.blade.php]
   */
  public function show(Post $post) {
    return View::make('posts.show', compact('post'));
  }
  /**
   * Liste des posts
   * @return [index]      [vue posts/index.blade.php]
   */
  public function index() {
    $posts = Post::latest()->get();
    return response()->view('posts.rss', compact('posts'))->header('Content-Type', 'text/xml');
  }
}
