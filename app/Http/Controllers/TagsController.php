<?php
// ./app/Http/Controllers/TagsController.php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use App\Models\Tag;

class TagsController extends Controller {

  /**
   * Détails d'un tag
   * @param  integer $tag [détails d'un tag à afficher]
   * @return [view]      [vue tags/show.blade.php]
   */
  public function show(Tag $tag) {
    return View::make('tags.show', compact('tag'));
  }
}
