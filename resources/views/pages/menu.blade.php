{{--
  resources/views/pages/menu.blade.php
  Description: Menu des pages
  Variable disponibles :
  - $pages: ARRAY(OBJ(id, titreMenu, titrePage, titreContenu, sousTitreContenu, description))
--}}

<ul class="nav navbar-nav navbar-right">
  @foreach ($pages as $page)
    <li class="{{ (request()->segment(2) == $page->id) ? 'active' : '' }}">
      <a href="{{ URL::route('pages.show', ['page' => $page->id, 'slug' => Str::slug($page->titreMenu, '-')]) }}">{{ $page->titreMenu }}</a>
    </li>
  @endforeach
</ul>
