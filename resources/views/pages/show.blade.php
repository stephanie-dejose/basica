{{--
  resources/views/pages/show.blade.php
  Description: Détails d'une page
  Variable disponibles :
  $page: OBJ(id, titreMenu, titrePage, titreContenu, sousTitreContenu, description)
  $i $page->id = 1 : $projets : ARRAY(OBJ(id, titre, description, client, image, created_at, updated_at, slider))
--}}

@extends('template.defaut')

@section('titre')
  {{ $page->titrePage }}
@stop

@section('contenu')
  @if($page->id === 1)
    @include('projets.slider')
    @include('projets.index')
    @include('posts.index')
  @elseif($page->id === 2)
    @include('projets.index')
    @section('scripts')
      <script src="{{ asset('js/projets/index.js') }}"></script>
    @stop
  @elseif ($page->id === 3)
    @include('posts.index')
  @elseif($page->id === 4)
    @include('template.partials._contacts')
  @endif
@stop
