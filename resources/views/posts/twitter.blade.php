{{--
  ./resources/views/posts/show.blade.php
  Description: Derniers posts Twitter
  Données disponibles :
      - $posts : OBJ(id, titre, texte, created_at, updated_at, image, categorie_id)
 --}}
 <!-- Latest News Twitter -->
 <div class="col-sm-6 latest-news">
   <h2>Latest Twitter News</h2>
   <a class="twitter-timeline" data-width="425" data-height="400" href="https://twitter.com/Web_Worker7?ref_src=twsrc%5Etfw">Tweets by Web_Worker7</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
 </div>
 <!-- End Featured News -->
