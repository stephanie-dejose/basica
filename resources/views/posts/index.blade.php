{{--
  ./resources/views/posts/index.blade.php
  Description: Liste des posts
  Données disponibles :
      - $posts : ARRAY(OBJ(id, titre, texte, created_at, updated_at, image, categorie_id))
--}}

@if($page->id === 1)
  <div class="section">
    <div class="container">
      <div class="row">
        <!-- List of latest posts -->
        <div class="col-sm-6 featured-news">
          <h2>Latest Blog Posts</h2>
          @foreach ($posts as $post)
            <div class="row">
              <div class="col-xs-4"><a href="{{ URL::route('posts.show', ['post' => $post->id, 'slug' => Str::slug($post->titre, '-')]) }}"><img src="{{ asset('img/blog/'.$post->image) }}" alt="Post Title"></a></div>
              <div class="col-xs-8">
                <div class="caption"><a href="{{ URL::route('posts.show', ['post' => $post->id, 'slug' => Str::slug($post->titre, '-')]) }}">{{ $post->titre }}</a></div>
                <div class="date">{{ \Carbon\Carbon::parse($post->created_at)->format('d M, Y') }}</div>
                <div class="intro">
                  {{ Str::words($post->texte, 20, '... ') }}
                  <br/>
                  <a href="{{ URL::route('posts.show', ['post' => $post->id, 'slug' => Str::slug($post->titre, '-')]) }}">Read more</a>
                </div>
              </div>
            </div>
          @endforeach
        </div>
        <!-- Latest Twitter News -->
        @include('posts.twitter')
      </div>
    </div>
  </div>

@elseif($page->id === 3)
<div class="section section-breadcrumbs">
  <div class="container">
    <div class="row">
      <!-- Page Title -->
      <div class="col-md-12">
        <h1>Our Blog</h1>
      </div>
    </div>
  </div>
</div>

  <div class="section">
    <div class="container">
      <div class="row">
        <!-- Blog Post Excerpt -->
        @foreach ($posts as $post)
          <div class="col-sm-6">
            <div class="blog-post blog-single-post">
              <div class="single-post-title">
                <h2>{{ $post->titre }}</h2>
              </div>

              <div class="single-post-image">
                <img src="{{ asset('img/blog/'.$post->image) }}" alt="Post Title">
              </div>

              <div class="single-post-info">
                <i class="glyphicon glyphicon-time"></i>{{ \Carbon\Carbon::parse($post->created_at)->format('d M, Y') }}
              </div>

              <div class="single-post-content">
                <p>
                  {{ Str::words($post->texte, 50, '... ') }}
                </p>
              <a href="{{ URL::route('posts.show', ['post' => $post->id, 'slug' => Str::slug($post->titre, '-')]) }}" class="btn">Read more</a>
              </div>
            </div>
          </div>
        @endforeach
        <!-- End Blog Post Excerpt -->

        <!-- Pagination -->
        <div class="pagination-wrapper ">
          <ul class="pagination pagination-sm">
            {!! $posts->links(); !!}
          </ul>
        </div>

      </div>
    </div>
  </div>
@endif;
