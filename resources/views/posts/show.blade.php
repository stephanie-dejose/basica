{{--
  ./resources/views/posts/show.blade.php
  Description: Détail d'un post
  Données disponibles :
      - $posts : OBJ(id, titre, texte, created_at, updated_at, image, categorie_id)
 --}}

 @extends('template.defaut')
 @section('titre')
   {{ $post->titre }}
 @stop

 @section('contenu')
  <div class="section section-breadcrumbs">
    <div class="container">
      <div class="row">
        <!-- Page Title -->
        <div class="col-md-12">
          <h1>Blog Post</h1>
        </div>
      </div>
    </div>
  </div>

  <div class="section">
    <div class="container">
      <div class="row">
        <!-- Blog Post -->
        <div class="col-sm-8">
          <div class="blog-post blog-single-post">
            <div class="single-post-title">
              <h2>{{ $post->titre }}</h2>
            </div>
            <div class="single-post-image">
              <img src="{{ asset('img/blog/'.$post->image) }}" alt="Post Title">
            </div>
            <div class="single-post-info">
              <i class="glyphicon glyphicon-time"></i>{{ \Carbon\Carbon::parse($post->created_at)->format('d M, Y') }}
              <br/>
              Category : <a href="{{ URL::route('categories.show', ['categorie' => $post->categorie->id, 'slug' => Str::slug($post->categorie->nom, '-')]) }}">{{ $post->categorie->nom }}</a>
            </div>
            <div class="single-post-content">
              <h3>{{ $post->titre }}</h3>
              <p>
                {{ $post->texte }}
              </p>
            </div>
          </div>
        </div>
        <!-- End Blog Post -->

        <!-- Sidebar -->
        <div class="col-sm-4 blog-sidebar">
          <h4>Recent Posts</h4>
          <ul class="recent-posts">
            @include('posts.latest')
          </ul>

          <h4>Categories</h4>
          <ul class="blog-categories">
             @include('categories.liste')
          </ul>
        </div>
        <!-- End Sidebar -->
      </div>
    </div>
  </div>
 @stop
