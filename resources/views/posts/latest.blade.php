{{--
  ./resources/views/posts/latest.blade.php
  Description: Liste des derniers posts
  Données disponibles :
        - $posts : ARRAY(OBJ(id, titre, texte, created_at, updated_at, image, categorie_id))
 --}}

@foreach ($posts as $post)
  <li>
    <a href="{{ URL::route('posts.show', ['post' => $post->id, 'slug' => Str::slug($post->titre, '-')]) }}">{{ $post->titre }}</a>
  </li>
@endforeach
