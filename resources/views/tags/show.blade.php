{{--
  ./resources/views/tags/show.blade.php
  Description : Liste des projets liée à un tag choisi
  variables disponibles :
      - $tag Tag
--}}
@extends('template.defaut')

@section('titre')
  Projets du {{ $tag->nom }}
@endsection

@section('contenu')
  <div class="section section-breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h1>Liste de projets pour {{ $tag->nom }}</h1>
        </div>
      </div>
    </div>
  </div>
  <div class="section">
  <div class="container">
  <div class="row">
        <ul class="grid cs-style-2">
          @foreach($tag->projets as $projet)
            <div class="col-md-4 col-sm-6">
              <figure>
                <img src="{{ asset('img/portfolio/'.$projet->image) }}" alt="">
                <figcaption>
                  <h3>{{ $projet->titre }}</h3>
                  <span>{{ $projet->client }}</span>
                  <a href="{{ URL::route('projets.show', ['projet' => $projet->id, 'slug' => Str::slug($projet->titre, '-')]) }}">Take a look</a>
                </figcaption>
              </figure>
            </div>
          @endforeach
        </ul>

      </div>
    <!-- End Blog Post -->

 </div>
 </div>

@endsection
