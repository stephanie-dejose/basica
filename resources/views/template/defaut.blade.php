{{--
  ./resources/views/template/defaut.blade.php
  Template général
  Variables disponibles:
  /
--}}
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]> <html class="no-js"> <![endif]-->
<html lang="en">
  <head>
  @include('template.partials._head')
  </head>

  <body data-baseURL="{{ url('/') }}">
    <!--[if lt IE 7]>
      <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
    <![endif]-->

    <!-- HEADER -->
    @include('template.partials._header')

    <!-- CONTENT -->
    @section('contenu')
    @show

  	<!-- FOOTER -->
    @include('template.partials._footer')

    <!-- SCRIPTS -->
    @include('template.partials._scripts')
  </body>
</html>
