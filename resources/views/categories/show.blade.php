{{--
  ./resources/views/categories/show.blade.php
  Description: Liste des posts pour une catégorie choisie
  variables disponibles :
      - $categorie Categorie
 --}}
@extends('template.defaut')

@section('titre')
  Projets du {{ $categorie->nom }}
@endsection

@section('contenu')
  <div class="section section-breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h1>Liste de posts pour {{ $categorie->nom }}</h1>
        </div>
      </div>
    </div>
  </div>

  <div class="section">
    <div class="container">
      <div class="row">
        <!-- Blog Post -->
        @foreach($categorie->posts as $post)
          <div class="col-sm-6">
            <div class="blog-post blog-single-post">
              <div class="single-post-title">
                <h2>{{ $post->titre }}</h2>
              </div>

              <div class="single-post-image">
                <img src="{{ asset('img/blog/'.$post->image) }}" alt="Post Title">
              </div>

              <div class="single-post-info">
                <i class="glyphicon glyphicon-time"></i>{{ \Carbon\Carbon::parse($post->created_at)->format('d M, Y') }}
              </div>

              <div class="single-post-content">
                <p>
                  {{ Str::words($post->texte, 50, '... ') }}
                </p>
                <a href="{{ URL::route('posts.show', ['post' => $post->id, 'slug' => Str::slug($post->titre, '-')]) }}" class="btn">Read more</a>
              </div>
            </div>
          </div>
        @endforeach
        <!-- End Blog Post -->
      </div>
    </div>
  </div>
@endsection
