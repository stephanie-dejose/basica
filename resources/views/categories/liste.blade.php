{{--
  ./resources/views/categories/liste.blade.php
  Description: Liste des catégories
  Données disponibles :
      - $categories : ARRAY(id, nom, created_at, updated_at)
 --}}

 @foreach ($categories as $categorie)
   <li>
     <a href="{{ URL::route('categories.show', ['categorie' => $categorie->id, 'slug' => Str::slug($categorie->nom, '-')]) }}">
       {{ $categorie->nom }}
     </a>
   </li>
 @endforeach
