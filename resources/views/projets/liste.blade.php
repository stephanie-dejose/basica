{{--
  ./resources/views/projets/liste.blade.php
  Description: Liste détails d'un projet
  Données disponibles :
      - $projets : ARRAY(OBJ(id, titre, description, client, image, created_at, updated_at, slider))
 --}}

@foreach ($projets as $projet)
  <div class="col-md-4 col-sm-6 projet">
    <figure>
      <img src="{{ asset('img/portfolio/'.$projet->image) }}" alt="">
      <figcaption>
        <h3>{{ Str::limit($projet->titre, 17, '... ') }}</h3>
        <span>{{ $projet->client }}</span>
        <a href="{{ URL::route('projets.show', ['projet' => $projet->id, 'slug' => Str::slug($projet->titre, '-')]) }}">Take a look</a>
      </figcaption>
    </figure>
  </div>
@endforeach
