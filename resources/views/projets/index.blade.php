{{--
  ./resources/views/projets/index.blade.php
  Description: Liste des projets
  Données disponibles :
      - $projets : ARRAY(OBJ(id, titre, description, client, image, created_at, updated_at, slider))
 --}}
@if($page->id === 1)
  <div class="section section-white">
    <div class="container">
      <div class="row">
        <!-- Page Title -->
        <div class="section-title">
          <h1>{{ $page->titrePage }}</h1>
        </div>
        <!-- Latest Projects -->
        <ul class="grid cs-style-3" id="liste-projets">
          @include('projets.liste')
        </ul>
      </div>
    </div>
  </div>
  <hr>
@elseif($page->id === 2)
  <div class="section section-breadcrumbs">
    <div class="container">
      <div class="row">
        <!-- Page Title -->
        <div class="col-md-12">
          <h1>{{ $page->titrePage }}</h1>
        </div>
      </div>
    </div>
  </div>

  <div class="section">
    <div class="container">
      <div class="row">
        <!-- Page Content Title / Content Subtitle / Description -->
        <div class="col-sm-12">
          <h2>{{ $page->titreContenu }}</h2>
          <h3>{{ $page->sousTitreContenu }}</h3>
          <p>
            {{ $page->description }}
          </p>
        </div>
        <!-- Latest Projects -->
        <div class="row">
          <ul class="grid cs-style-3" id="liste-projets">
            @include('projets.liste')
          </ul>
        </div>
        <!-- More works button -->
        <ul class="pager">
          <li><a href="#" id="older-projects">More works</a></li>
        </ul>
      </div>
    </div>
  </div>
@endif
