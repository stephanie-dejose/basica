{{--
  ./resources/views/projets/_slider.blade.php
  Description : Slider des projets
--}}

<section id="main-slider" class="no-margin">
    <div class="carousel slide">
        <ol class="carousel-indicators">
          @foreach($projets as $projet)
            <li data-target="#main-slider" data-slide-to="{{ $loop->index }}" class="{{ $loop->first ? 'active' : '' }}"></li>
          @endforeach
        </ol>
        <div class="carousel-inner">
          @foreach($projets as $projet)
            <div class="item {{ $loop->first ? 'active' : '' }}" style="background-image: url({{ asset('img/portfolio/'.$projet->image) }})">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="carousel-content centered">
                                <h2 class="animation animated-item-1">{{ $projet->titre }}</h2>
                                <p class="animation animated-item-2">{{ Str::words($projet->description, 30, '... ') }}</p>
                                <br>
                                <a class="btn btn-md animation animated-item-3" href="{{ URL::route('projets.show', ['projet' => $projet->id, 'slug' => Str::slug($projet->titre, '-')]) }}">Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/.item-->
          @endforeach

        </div><!--/.carousel-inner-->
    </div><!--/.carousel-->
    <a class="prev hidden-xs" href="#main-slider" data-slide="prev">
        <i class="icon-angle-left"></i>
    </a>
    <a class="next hidden-xs" href="#main-slider" data-slide="next">
        <i class="icon-angle-right"></i>
    </a>
</section><!--/#main-slider-->
