{{--
  ./resources/views/projets/show.blade.php
  Description: Détails d'un projet
  Variable disponibles :
  $projet: OBJ(id, titre, description, client, image, created_at, updated_at, slider)
--}}
@extends('template.defaut')
@section('titre')
  {{ $projet->titre }}
@stop

@section('contenu')
  <div class="section section-breadcrumbs">
    <div class="container">
      <div class="row">
        <!-- Page Title -->
        <div class="col-md-12">
          <h1>Project Details</h1>
        </div>
      </div>
    </div>
  </div>

  <div class="section">
    <div class="container">
      <div class="row">
        <!-- Product Image & Available Colors -->
        <div class="col-sm-6">
          <div class="product-image-large">
            <img src="{{ asset('img/portfolio/'.$projet->image) }}" alt="Item Name">
          </div>
          <div class="colors">
            <span class="color-white"></span>
            <span class="color-black"></span>
            <span class="color-blue"></span>
            <span class="color-orange"></span>
            <span class="color-green"></span>
          </div>
        </div>
        <!-- End Product Image & Available Colors -->
        <!-- Product Summary & Options -->
        <div class="col-sm-6 product-details">
          <h2>{{ $projet->titre }}</h2>
          <h3>Quick Overview</h3>
          <p>
            {{ $projet->description }}
          </p>
          <h3>Project Details</h3>
          <p><strong>Client: </strong>{{ $projet->client }}</p>
          <p><strong>Date: </strong>{{ \Carbon\Carbon::parse($projet->created_at)->format('d M, Y') }}</p>
          <p><strong>Tags: </strong>
            @foreach ($projet->tags as $tag)
            <a href="{{ route('tags.show', [
                              'tag'  => $tag->id,
                              'slug' => Str::slug($tag->nom, '-')
                              ])}}">
                     {{ $tag->nom }}</a>
        @endforeach</p>
        </div>
        <!-- End Product Summary & Options -->

      </div>
    </div>
  </div>
  <hr>
  <div class="section">
    <div class="container">
      <div class="row">

        <div class="section-title">
          <h1>Similar Works</h1>
        </div>

        <ul class="grid cs-style-2">
          @foreach ($projetsSimilaires as $projetsSimilaire)
          <div class="col-md-3 col-sm-6">
            <figure>
              <img src="{{ asset('img/portfolio/'.$projetsSimilaire->image) }}" alt="img">
              <figcaption>
                <h3>{{ Str::limit($projetsSimilaire->titre, 9, '...') }}</h3>
                <span>{{ $projetsSimilaire->client }}</span>
                <a href="{{ URL::route('projets.show', ['projet' => $projetsSimilaire->id, 'slug' => Str::slug($projetsSimilaire->titre, '-')]) }}">Take a look</a>
              </figcaption>
            </figure>
          </div>
          @endforeach
        </ul>

      </div>
    </div>
  </div>
@stop
