<?php
/*
  ./routes/web.php
  Routeur principal
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
*/

//  ROUTES AJAX      ===========================================================
/*
  CHARGEMENT DES OLDER POSTS
  PATTERN: /ajax/older-posts
  CTRL: Projets
  ACTION: ajaxOlders
 */
Route::get('/ajax/older-projects', 'ProjetsController@ajaxOlders')->name('projets.ajax.olders');

//  ROUTES STANDARDS ===========================================================

/*-----------------------------------
    PROJETS
-----------------------------------*/

Route::prefix('projets')->name('projets.')->group(function(){
/*
  DETAILS D'UN PROJET
  PATTERN: /projets/{projet}/{slug}.html [GET]
  CTRL: Projets
  ACTION: show
*/
Route::get('{projet}/{slug}.html', 'ProjetsController@show')
      ->where([
               'projet'   => '[1-9][0-9]*',
               'slug' => '[a-z0-9][a-z0-9\-]*'
             ])
      ->name('show');
});

/*-----------------------------------
    POSTS
-----------------------------------*/

Route::prefix('posts')->name('posts.')->group(function(){
/*
  DETAILS D'UN POST
  PATTERN: /posts/{post}/{slug}.html [GET]
  CTRL: Posts
  ACTION: show
*/

Route::get('{post}/{slug}.html', 'PostsController@show')
      ->where([
               'post'   => '[1-9][0-9]*',
               'slug'   => '[a-z0-9][a-z0-9\-]*'
             ])
      ->name('show');
/*
  FLUX RSS - DERNIERS POSTS BASICA
  PATTERN: /posts/rss [GET]
  CTRL: PostsController
  ACTION: rss
*/
Route::get('rss', 'PostsController@index')->name('rss');

});

/*-----------------------------------
    TAGS
-----------------------------------*/
/*
  DETAILS D'UN TAG
  PATTERN: /tags/{tag}/{slug}.html [GET]
  CTRL: Tags
  ACTION: show
 */
Route::get('/tags/{tag}/{slug}.html', 'TagsController@show')
      ->where([
               'tag'   => '[1-9][0-9]*',
               'slug' => '[a-z0-9][a-z0-9\-]*'
             ])
      ->name('tags.show');

/*-----------------------------------
    CATEGORIES
-----------------------------------*/
/*
 DETAILS D'UNE CATEGORIE
 PATTERN: /categories/{categorie}/{slug}.html [GET]
 CTRL: Categories
 ACTION: show
*/
Route::get('/categories/{categorie}/{slug}.html', 'CategoriesController@show')
      ->where([
              'categorie'   => '[1-9][0-9]*',
              'slug' => '[a-z0-9][a-z0-9\-]*'
            ])
      ->name('categories.show');


/*-----------------------------------
    PAGES
-----------------------------------*/
/*
 DETAILS D'UNE PAGE
 PATERN: /pages/{page}/{slug}.html
 CTRL: PagesController
 ACTION: show
*/
Route::get('/pages/{page}/{slug}.html', 'PagesController@show')
     ->where([
               'page'   => '[1-9][0-9]*',
               'slug'   => '[a-z0-9][a-z0-9\-]*'
             ])
      ->name('pages.show');



/*-------------------------------------------------
  MISE A DISPOSITION DE DONNEES POUR CERTAINES VUES
-------------------------------------------------*/

View::composer('pages.menu', function($view){
  $view->with('pages', App\Models\Page::all());
});

View::composer('posts.latest', function($view){
  $view->with('posts', App\Models\Post::orderBy('created_at', 'desc')->take(4)->get());
});

View::composer('categories.liste', function($view){
  $view->with('categories', App\Models\Categorie::orderBy('nom', 'asc')->get());
});

View::composer('projets.slider', function($view){
  $view->with('projets', App\Models\Projet::where('slider', '=', 1)->get());
});

/*
  ROUTE PAR DEFAUT
  PATERN: /
  CTRL: PagesController
  ACTION: show
 */
Route::get('/', 'PagesController@show')->name('accueil');
