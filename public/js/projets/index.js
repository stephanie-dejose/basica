/*
  ./public/js/projets/index.js
*/

$(function(){
  var offsetCalcule = 6;
  var baseURL = $('body').attr('data-baseURL');
  $('#older-projects').click(function(e){
    e.preventDefault();
    $.ajax({
      url: baseURL + '/ajax/older-projects',
      data: {
        offset: offsetCalcule
      },
      method: 'get',
      success: function(reponsePHP){
        $('#liste-projets').append(reponsePHP)
                           .find('.projet:nth-last-of-type(-n+6)')
                           .hide()
                           .fadeIn();
        // offsetCalcule = offsetCalcule + 10;
        offsetCalcule += 6;
      },
      error: function(){
        alert("Problème durant la transaction !");
      }
    });
  });
});
